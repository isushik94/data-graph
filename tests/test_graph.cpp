#include <cxxtest/TestSuite.h>

#include <data-graph/engine.hpp>

class test_graph : public CxxTest::TestSuite
{
    using scalar_type = dg::decl_data_type<int, float, double>;

    using add_op = dg::decl_operation<scalar_type(scalar_type, scalar_type)>;

    using engine = dg::engine<dg::use_data_types<scalar_type>, dg::use_operations<add_op>>;

    static_assert(engine::n_data_types == 2, "Unexpected value for engine::n_types");
    static_assert(engine::n_operations == 3, "Unexpected value for engine::n_operations");
    static_assert(engine::max_n_subtypes == 3, "Unexpected value for engine::max_n_subtypes");
    static_assert(engine::max_n_arguments == 3, "Unexpected value for engine::max_n_arguments");

public:
    void test_create_node()
    {
        engine::graph g;
        {
            // input node
            {
                auto n = g.identity<scalar_type>();

                TS_ASSERT(n.is<scalar_type>());
            }

            // input node with default value
            {
                auto n = g.identity<scalar_type>(1);

                TS_ASSERT(n.is<scalar_type>());
            }

            // opeartion node with const input
            {
                auto n = g.operation<add_op>(1, 1.);

                TS_ASSERT(n.is<scalar_type>());
            }
        }
    }

    void test_simple_graph()
    {
        /*
            [inp1]   [inp2]
               \       /
                 \   /
                  (+)
                   |
                   v
                 [out]
         */
        engine::graph g;
        {
            auto inp1 = g.identity<scalar_type>();

            auto inp2 = g.identity<scalar_type>();

            auto out = g.operation<add_op>(inp1, inp2);
            {
                TS_ASSERT(out.is<scalar_type>());
            }
        }
    }
};
