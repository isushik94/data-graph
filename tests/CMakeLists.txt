cmake_minimum_required(VERSION 3.0)

set(TESTS test_engine test_graph test_device)

find_package(CxxTest)

if(NOT CXXTEST_FOUND)
    message("`BUILD_TESTS` is set but CxxTest hasn't found. Tests disabled.")
elseif(CXXTEST_FOUND)
    function(_ADD_TEST test_name)
        CXXTEST_ADD_TEST(${test_name} ${test_name}.cc ${CMAKE_CURRENT_SOURCE_DIR}/${test_name}.cpp)
        target_link_libraries(${test_name} ${DG_LIBRARY_TARGET})
        #        target_include_directories(${test_name} PRIVATE ${LIB_INCLUDE_DIRS})
        #        target_link_libraries(${test_name} PRIVATE ${arg})
    endfunction()

    include_directories(${CXXTEST_INCLUDE_DIR})

    set(_TESTS )

    foreach(_TEST_NAME ${TESTS})
        _add_test(${_TEST_NAME})

        list(APPEND _TESTS ${_TEST_NAME})
        message("Configuring test `${_TEST_NAME}`")
    endforeach()

    set_tests_properties(${_TESTS} PROPERTIES RUN_SERIAL TRUE)
    foreach(_TEST ${_TESTS})
        foreach(_LIBRARY_DIR ${LIBRARIES_LIBRARY_DIRS})
            set_property(TEST ${_TEST} APPEND PROPERTY ENVIRONMENT LD_LIBRARY_PATH=${_LIBRARY_DIR})
        endforeach()
    endforeach()
endif()
