#include <cxxtest/TestSuite.h>

#include <data-graph/engine.hpp>

#include <iostream>

class test_engine : public CxxTest::TestSuite
{
    using scalar_type = dg::decl_data_type<int, float, double>;
    struct string_type : dg::decl_data_type<std::string>
    {
        static constexpr std::string_view name = "string";
    };

    using add_op = dg::decl_operation<scalar_type(scalar_type, scalar_type)>;

    using engine = dg::engine<dg::use_data_types<scalar_type, string_type>, dg::use_operations<add_op>>;

    static_assert(engine::n_data_types == 3, "Unexpected value for engine::n_types");
    static_assert(engine::n_operations == 3, "Unexpected value for engine::n_operations");
    static_assert(engine::max_n_subtypes == 3, "Unexpected value for engine::max_n_subtypes");
    static_assert(engine::max_n_arguments == 3, "Unexpected value for engine::max_n_arguments");

public:
    void test_type_names()
    {
        std::string_view expected_type_names[] = { "type_0", "string" };

        auto& engine_data_type_names = engine::data_type_names;

        // 1 predefined type
        TS_ASSERT_EQUALS(std::size(engine_data_type_names) - 1, std::size(expected_type_names));

        auto e = std::min(std::size(engine_data_type_names) - 1, std::size(expected_type_names));
        for (std::size_t i = 0; i < e; i++)
        {
            TS_ASSERT_EQUALS(engine_data_type_names[i + 1].compare(expected_type_names[i]), 0);
        }
    }

    void test_operation_names()
    {
        std::string_view expected_operation_names[] = { "operation_0" };

        auto& engine_operation_names = engine::operation_names;

        // 2 predefined ops
        TS_ASSERT_EQUALS(std::size(engine_operation_names) - 2, std::size(expected_operation_names));

        auto e = std::min(std::size(engine_operation_names) - 2, std::size(expected_operation_names));
        for (std::size_t i = 0; i < e; i++)
        {
            TS_ASSERT_EQUALS(engine_operation_names[i + 2].compare(expected_operation_names[i]), 0);
        }
    }
};
