#ifndef __STRING_HPP__
#define __STRING_HPP__

#include <string_view>
#include <utility>

#include <data-graph/type_traits.hpp>

namespace _details
{
    template <std::uint8_t Base>
    struct base
    {
        template <std::size_t Rem, std::size_t... Digits>
        struct to_digits_impl : public to_digits_impl<Rem / Base, Rem % Base, Digits...>
        {};

        template <std::size_t... Digits>
        struct to_digits_impl<0, Digits...>
        {
            using type = std::integer_sequence<std::uint8_t, Digits...>;
        };

        template <std::size_t P>
        static constexpr std::size_t pow()
        {
            if constexpr (P == 0)
                return 1;
            else
                return std::size_t(Base) * pow<P - 1>();
        }

        template <std::size_t Digit, std::size_t P>
        static constexpr std::size_t convert_digit()
        {
            return Digit * pow<P>();
        }

        template <auto... Digits>
        struct from_digits_impl
        {
            template <class Indices>
            struct enumerate;

            template <std::size_t... Is>
            struct enumerate<std::index_sequence<Is...>>
            {
                static constexpr std::size_t value =
                    reduce_v<reduce_ops::sum,
                             0,
                             convert_digit<std::size_t(Digits), sizeof...(Digits) - 1 - Is>()...>;
            };

            static constexpr std::size_t value =
                enumerate<std::make_index_sequence<sizeof...(Digits)>>::value;
        };
    };
}   // namespace _details

template <std::uint8_t Base>
struct base
{
    template <std::size_t Number>
    struct to_digits
    {
        using type =
            typename _details::template base<Base>::template to_digits_impl<Number / Base,
                                                                            Number % Base>::type;
    };

    template <std::size_t Number>
    using to_digits_t = typename to_digits<Number>::type;

    template <class DigitsSeq>
    struct from_digits;

    template <typename T, T... Digits>
    struct from_digits<std::integer_sequence<T, Digits...>>
    {
        static constexpr std::size_t value =
            _details::template base<Base>::template from_digits_impl<Digits...>::value;
    };

    template <class DigitsSeq>
    static constexpr std::size_t from_digits_v = from_digits<DigitsSeq>::value;
};

using base10 = base<10>;
using base16 = base<16>;

//

namespace _details
{
}

template <class DigitsSeq>
struct to_chars;

template <typename T, T... Digits>
struct to_chars<std::integer_sequence<T, Digits...>>
{
    using type = std::integer_sequence<char, ('0' + Digits)...>;

    static constexpr const char value[] = { ('0' + Digits)..., '\0' };

    static constexpr std::size_t size() { return type::size(); }

    template <char... Prefix>
    struct with_prefix
    {
        using type = std::integer_sequence<char, Prefix..., ('0' + Digits)...>;

        static constexpr const char value[] = { Prefix..., ('0' + Digits)..., '\0' };

        static constexpr std::size_t size() { return type::size(); }

        template <char... Suffix>
        struct with_suffix
        {
            using type = std::integer_sequence<char, Prefix..., ('0' + Digits)..., Suffix...>;

            static constexpr const char value[] = { Prefix..., ('0' + Digits)..., Suffix..., '\0' };

            static constexpr std::size_t size = type::size();
        };
    };

    template <char... Suffix>
    struct with_suffix
    {
        using type = std::integer_sequence<char, ('0' + Digits)..., Suffix...>;

        static constexpr const char value[] = { ('0' + Digits)..., Suffix..., '\0' };

        static constexpr std::size_t size() { return type::size(); }
    };
};

#endif
