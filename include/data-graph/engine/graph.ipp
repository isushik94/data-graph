
#ifndef __DATA_GRAPH__ENGINE__GRAPH_IPP__
#define __DATA_GRAPH__ENGINE__GRAPH_IPP__

#include "./graph.hpp"   // for IDEs

namespace dg
{
    //
    // graph
    //

    //

    template <class Engine>
    graph<Engine>::graph() : graph({}) {}

    template <class Engine>
    graph<Engine>::graph(allocator_type alloc) : base_type(alloc) {}

    // [[nodiscard]] node<DataType> identity()

    template <class Engine>
    template <class DataType>
    [[nodiscard]] typename graph<Engine>::template node<DataType> graph<Engine>::identity()
    {
        // static checking of arguments
        {
            static_assert(engine_type::template data_type_is_allowed<DataType>(),
                          "Provided `DataType` is not registered in the engine");
        }

        node<DataType> node_out(*this);
        base_type::place_identity(node_out);
        return node_out;
    }

    // [[nodiscard]] node<DataType> identity(ArgNode&& node_in)

    template <class Engine>
    template <class DataType, typename ArgNode>
    [[nodiscard]] typename graph<Engine>::template node<DataType> graph<Engine>::identity(ArgNode&& node_in)
    {
        // static checking of arguments
        {
            static_assert(engine_type::template data_type_is_allowed<DataType>(),
                          "Provided `Type` is not registered in the engine.");

            static_assert(_validate_argument<DataType, std::decay_t<ArgNode>>(), "Invalid argument");
        }

        node<DataType> node_out(*this);
        base_type::place_identity(node_out, std::forward<ArgNode>(node_in));
        return node_out;
    }

    // [[nodiscard]] auto operation(NodeTypes&&... nodes)

    template <class Engine>
    template <class Operation, typename... NodeTypes>
    [[nodiscard]] auto graph<Engine>::operation(NodeTypes&&... nodes)
    {
        // static checking of arguments
        {
            static_assert(engine_type::template operation_is_allowed<Operation>(),
                          "Provided operations is not registered in the engine");

            using input_types = typename engine_type::template operation_get_input_types<Operation>;

            static_assert(sizeof...(NodeTypes) == input_types::size(),
                          "The number of provided arguments doesn't meet requirements of the operation.");

            using arg_pairs_type = types_list_zip_t<input_types, types_list<std::decay_t<NodeTypes>...>>;

            static_for<arg_pairs_type::size()>([](auto i) {
              using arg_pair_type = typename arg_pairs_type::template at_t<i>;

              using op_data_type = typename arg_pair_type::template at_t<0>;
              using node_type    = typename arg_pair_type::template at_t<1>;

              static_assert(_validate_argument<op_data_type, std::decay_t<node_type>, i>(), "Invalid argument");
            });
        }

        using operation_output_type = typename engine_type::template operation_get_output_type<Operation>;

        node<operation_output_type> node_out(*this);
        base_type::template place_operation<Operation>(node_out, std::forward<NodeTypes>(nodes)...);
        return node_out;
    }

    // static constexpr bool _validate_argument()

    template <class Engine>
    template <class DataType, class ArgNode, std::size_t I /* = 0 */>
    constexpr bool graph<Engine>::_validate_argument()
    {
        constexpr auto is_node_argument = base_type::graph_helpers::template is_node_v<ArgNode>;
        if constexpr (is_node_argument)
        {
            using ArgNode_type = typename base_type::graph_helpers::template get_node_data_type_t<ArgNode>;

            // checks if `ArgNode`'s data type is `DataType`
            constexpr auto is_same_data_type = std::is_same_v<DataType, ArgNode_type>;
            static_assert(is_same_data_type,
                          "The provided node as an argument has another type than the called identity operation.");
            return is_same_data_type;
        }
        else
        {
            // checks if `ArgNode` is a `DataType`'s subtype
            constexpr auto is_subtype = engine_type::template data_type_get_subtypes<DataType>::template contains<ArgNode>();
            static_assert(
                is_subtype,
                "The provided argument's type is unsupported. Please peek one of the specified in `decl_data_type` helper.");
            return is_subtype;
        }
    }

    //
    // node
    //

    template <class Engine>
    template <class DataType>
    graph<Engine>::node<DataType>::node(const node& o) : base_type(o)
    {}

    template <class Engine>
    template <class DataType>
    graph<Engine>::node<DataType>::node(graph& parent) : base_type(parent)
    {}
}

#endif