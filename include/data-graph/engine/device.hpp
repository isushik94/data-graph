
#ifndef __DATA_GRAPH__ENGINE__DEVICE_HPP__
#define __DATA_GRAPH__ENGINE__DEVICE_HPP__

#ifndef __DATA_GRAPH__ENGINE_HPP__
#    error "Please use `#include <data-graph/engine.hpp>` instead. This header is not supposed to be used directly."
#endif

#include <data-graph/engine/device/base.hpp>

namespace dg
{
    /**
     * @class device
     * @brief
     */

    template <class DataTypesList, class OperationsList, class Allocator>
    struct engine<DataTypesList, OperationsList, Allocator>::   //
        device final : detail::device_base<engine>
    {
        template <class Signature>
        struct pipeline;

        template <class DataType>
        struct value;

        using base_type = detail::device_base<engine>;

        using graph_t = typename engine::graph;

        template <class DataType>
        using graph_node_t = typename engine::graph::template node<DataType>;

        // TODO : add a support of node tuples

        /**
         * @brief
         *
         * @tparam DataType A node's data type
         * @param node
         * @return pipeline<DataType>
         */

        template <class DataType>
        [[nodiscard]] pipeline<DataType()> compile(const graph_node_t<DataType>& node)
        {
            pipeline<DataType()> pipe(*this);
            base_type::_impl_compile(pipe, node);
            return pipe;
        }

        /**
         * @brief
         *
         * @tparam DataType A node's data type
         * @tparam InputDataTypes
         * @param node
         * @return pipeline<DataType>
         */

        template <class DataType, class... InputDataTypes>
        [[nodiscard]] pipeline<DataType(InputDataTypes...)> compile(const graph_node_t<DataType>& node,
                                                                    graph_node_t<InputDataTypes>&... inputs)
        {
            pipeline<DataType(InputDataTypes...)> pipe(*this);
            base_type::_impl_compile(pipe, node, inputs...);
            return pipe;
        }

        /**
         * @brief
         *
         * @tparam Device A type of a new device
         * @tparams Args
         *
         * @param graph A graph for which a device will be created
         * @param args
         *
         * @return device A new device for the graph provided
         */

        template <class Device, class... Args>
        static device create(graph_t& graph, Args... args)
        {
            // static checking of arguments
            {
                static_assert(engine::device_is_allowed<Device>(),
                              "Provided operations is not registered in the engine");
            }

            // runtime checking of arguments

            if (!graph) throw std::invalid_argument("graph: must be valid");

            return device(graph, Device(std::forward<Args>(args)...));
        }

        device() {}
        ~device() {}

        device(device&& o) noexcept : base_type(std::move(o)) {}
        device& operator=(device&& o) noexcept
        {
            base_type::operator=(std::move(o));
            return *this;
        }

        device(const device& o) = delete;
        device& operator=(const device& o) = delete;

    private:
        template <class DeviceImpl>
        device(graph_t& graph, DeviceImpl&& impl) : base_type(graph, std::move(impl))
        {}
    };

    //

    template <class DataTypesList, class OperationsList, class Allocator>
    template <class Signature>
    struct engine<DataTypesList, OperationsList, Allocator>::   //
        device::pipeline final : detail::pipeline_base<engine, Signature>
    {
        friend struct engine::device;

        using base_type = detail::pipeline_base<engine, Signature>;

        template <class DataType>
        using value = typename device::template value<DataType>;

        pipeline()  = default;
        ~pipeline() = default;

        pipeline(pipeline&& o) noexcept : base_type(std::move(o)) {}
        pipeline& operator=(pipeline&& o) noexcept
        {
            base_type::operator=(std::move(o));
            return *this;
        }

        pipeline(const pipeline& o) = delete;
        pipeline& operator=(const pipeline& o) = delete;

        template <class... Args>
        value<typename base_type::output_type> operator()(Args&&... args)
        {
            // static checks
            {
                static_assert(sizeof...(Args) == base_type::inputs_types_list::size(),
                              "The number of provided arguments is not the same as in a signature of the pipeline.");
            }

            value<typename base_type::output_type> v;
            base_type::_impl_call(v, std::forward<Args>(args)...);
            return v;
        }

    protected:
        pipeline(device& dev) : base_type(dev) {}
    };

    /**
     * @class value
     * @brief
     *
     * @tparam DataType
     */

    template <class DataTypesList, class OperationsList, class Allocator>
    template <class DataType>
    struct engine<DataTypesList, OperationsList, Allocator>::   //
        device::value final : detail::value_base<engine, DataType>
    {
        static_assert(detail::engine_integrity::is_valid_data_type_v<DataType>, "DataType is invalid");

        friend struct engine::device;

        using base_type = detail::value_base<engine, DataType>;

        using data_type     = DataType;
        using subtypes_list = detail::engine_helpers::data_type::get_subtypes_t<data_type>;

        value() {}

        template <typename T>
        operator T() const
        {
            static_assert(subtypes_list::template contains<T>(), "Unsupported type `T` used as a subtype.");
            return base_type::template _impl_get<T>();
        }

        template <typename T>
        value& operator=(T&& t)
        {
            static_assert(subtypes_list::template contains<T>(), "Unsupported type `T` used as a subtype.");

            base_type::template _impl_set<T>(std::forward<T>(t));
            return *this;
        }

        engine::subtype_idx_type index() const { return base_type::_impl_subtype_index(); }

    protected:
        value(device& dev) : base_type(dev) {}
    };
}   // namespace dg

#endif
