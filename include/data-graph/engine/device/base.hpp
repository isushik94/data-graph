
#ifndef __DATA_GRAPH__DETAIL__DEVICE__BASE_HPP__
#define __DATA_GRAPH__DETAIL__DEVICE__BASE_HPP__

#include <list>
#include <memory>
#include <string_view>
#include <system_error>
#include <utility>

#include <data-graph/detail/engine/base.hpp>
#include <data-graph/detail/engine/helpers.hpp>

#include <data-graph/detail/graph/base.hpp>

#include <data-graph/detail/device/helpers.hpp>

namespace dg::detail
{
    /**
     * @class device_base
     * @brief
     *
     * @tparam Engine
     */

    template <class Engine>
    struct device_base_internal;

    template <class Engine>
    struct device_base : interface_unique::base<device_base_internal<Engine>>
    {
        static_assert(engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        using engine_type    = Engine;
        using device_helpers = detail::device_helpers<engine_type>;

        using graph_base = detail::graph_base<engine_type>;

        template <class DataType>
        using node_base = detail::node_base<engine_type, DataType>;

        template <class Signature>
        using pipeline_base = detail::pipeline_base<engine_type, Signature>;

        device_base(const device_base& o) = delete;
        device_base& operator=(const device_base& o) = delete;

    protected:
        device_base() = default;
        template <class DeviceImpl>
        device_base(graph_base& graph, DeviceImpl&& impl);
        ~device_base();

        device_base(device_base&& o) noexcept;
        device_base& operator=(device_base&& o) noexcept;

        template <class DataType, class... InputDataTypes>
        inline void _impl_compile(pipeline_base<DataType(InputDataTypes...)>& out,
                                  const node_base<DataType>& node,
                                  node_base<InputDataTypes>&... inputs);
    };

    /**
     * @class value_base
     * @brief
     *
     * @tparam Engine
     * @tparam DataType
     */

    template <class Engine, class DataType>
    struct value_base_internal;

    template <class Engine, class DataType>
    struct value_base : interface_inline::base<value_base_internal<Engine, DataType>>
    {
        using engine_type = Engine;
        using data_type   = DataType;

        using subtypes_list = detail::engine_helpers::data_type::get_subtypes_t<data_type>;

        friend struct device_base<Engine>;

    protected:
        value_base() = default;
        value_base(device_base<Engine>& dev);

        value_base(const value_base& o);
        value_base& operator=(const value_base& o);

        value_base(value_base&& o) noexcept;
        value_base& operator=(value_base&& o) noexcept;

        template <typename T, typename = std::enable_if_t<subtypes_list::template contains<T>()>>
        T _impl_get() const;

        template <typename T, typename = std::enable_if_t<subtypes_list::template contains<T>()>>
        void _impl_set(T&& t);

        typename engine_type::subtype_idx_type _impl_subtype_index() const;
    };

    /**
     * @class pipeline_base
     * @brief
     *
     * @tparam Engine
     * @tparam Signature
     */

    template <class Engine, class Signature>
    struct pipeline_base_internal;

    template <class Engine, class Signature>
    struct pipeline_base;

    template <class Engine, class RetType, class... ArgsTypes>
    struct pipeline_base<Engine, RetType(ArgsTypes...)>
        : interface_unique::base<pipeline_base_internal<Engine, RetType(ArgsTypes...)>>
    {
        static_assert(engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        static_assert(Engine::template data_type_is_allowed<std::decay_t<RetType>>(),
                      "Please make sure if the type, used for the node class, is registered in the engine.");

        static_assert(std::is_same_v<RetType, std::decay_t<RetType>>,
                      "Please use a data type without reference or pointer qualifiers.");

        friend struct device_base<Engine>;

        using output_type       = RetType;
        using inputs_types_list = types_list<ArgsTypes...>;

        pipeline_base(const pipeline_base& o) = delete;
        pipeline_base& operator=(const pipeline_base& o) = delete;

    protected:
        pipeline_base() = default;
        pipeline_base(device_base<Engine>& dev);

        pipeline_base(pipeline_base&& o) noexcept;
        pipeline_base& operator=(pipeline_base&& o) noexcept;

        template <class... Args>
        inline void _impl_call(value_base<Engine, output_type>& out, Args&&... args);
    };
}   // namespace dg::detail

#include "./_impl/base_common.ipp"

#include "./_impl/base_device.ipp"

#include "./_impl/base_pipeline.ipp"

#include "./_impl/base_value.ipp"

#endif
