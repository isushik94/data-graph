
namespace dg::detail
{
    //
    // device_base_internal
    //

    namespace _impl::kernel
    {
        /**
         * @struct initialize_context
         * @brief
         */

        template <class Engine>
        struct initialize_context
        {
            using operation_id_type = typename Engine::operation_id_type;

            operation_id_type operation_id;
        };

        /**
         * @struct compute_context
         * @brief
         */

        template <class Engine>
        struct compute_context
        {
            using operation_id_type = typename Engine::operation_id_type;

            using value_handle = detail::value_handle<Engine>;

            operation_id_type operation_id;

            std::vector<value_handle> inputs;

            value_handle output;
        };

        /**
         * @struct finalize_context
         * @brief
         */

        template <class Engine>
        struct finalize_context
        {
            using operation_id_type = typename Engine::operation_id_type;

            operation_id_type operation_id;
        };

        /**
         * @struct kernel_wrapper
         * @brief
         */

        template <class Engine>
        struct kernel_wrapper final
        {
            using operation_id_type = typename Engine::operation_id_type;

            /**
             * @typedef std::error_code (*initialize_ptr_type)(initialize_context&)
             * @brief A pointer to operation implementation kernel
             */
            typedef std::error_code (*initialize_ptr_type)(initialize_context<Engine>&);

            /**
             * @typedef std::error_code (*compute_ptr_type)(compute_context&)
             * @brief A pointer to operation implementation kernel
             */
            typedef std::error_code (*compute_ptr_type)(compute_context<Engine>&);

            /**
             * @typedef std::error_code (*finalize_ptr_type)(finalize_context&)
             * @brief A pointer to operation implementation kernel
             */
            typedef std::error_code (*finalize_ptr_type)(finalize_context<Engine>&);

            kernel_wrapper()
                : _operation_id(operation_id_type(-1)), _initialize(nullptr), _compute(nullptr), _finalize(nullptr)
            {}

            kernel_wrapper(operation_id_type op_id,
                           initialize_ptr_type init,
                           compute_ptr_type comp,
                           finalize_ptr_type fin)
                : _operation_id(op_id), _initialize(init), _compute(comp), _finalize(fin)
            {}

            operator bool() const noexcept { return !empty(); }

            [[nodiscard]] bool empty() const noexcept { return _operation_id == operation_id_type(-1); }

            operation_id_type operation_id() const noexcept { return _operation_id; }

            std::error_code initialize(initialize_context<Engine>& ctx) const
            {
                ctx.operation_id = _operation_id;
                return _initialize(ctx);
            }

            std::error_code compute(compute_context<Engine>& ctx) const
            {
                ctx.operation_id = _operation_id;
                return _compute(ctx);
            }

            std::error_code finalize(finalize_context<Engine>& ctx) const
            {
                ctx.operation_id = _operation_id;
                return _finalize(ctx);
            }

        private:
            operation_id_type _operation_id;

            ///
            initialize_ptr_type _initialize;

            ///
            compute_ptr_type _compute;

            ///
            finalize_ptr_type _finalize;
        };

        // kernel_initialize_base

        template <class Engine, class Operation, class OperationImpl, typename>
        struct kernel_initialize_base;

        template <class Engine, class Operation, class OperationImpl>
        struct kernel_initialize_base<Engine, Operation, OperationImpl, std::false_type>
        {
            static std::error_code initialize(initialize_context<Engine>& ctx) { return {}; }
        };

        template <class Engine, class Operation, class OperationImpl>
        struct kernel_initialize_base<Engine, Operation, OperationImpl, std::true_type>
        {
            static std::error_code initialize(initialize_context<Engine>& ctx) { return {}; }
        };

        // kernel_compute_base

        template <class Engine, class Operation, class OperationImpl>
        struct kernel_compute_base
        {
            static std::error_code compute(compute_context<Engine>& ctx) { return {}; }
        };

        // kernel_finalize_base

        template <class Engine, class Operation, class OperationImpl, typename>
        struct kernel_finalize_base;

        template <class Engine, class Operation, class OperationImpl>
        struct kernel_finalize_base<Engine, Operation, OperationImpl, std::false_type>
        {
            static std::error_code finalize(finalize_context<Engine>& ctx) { return {}; }
        };

        template <class Engine, class Operation, class OperationImpl>
        struct kernel_finalize_base<Engine, Operation, OperationImpl, std::true_type>
        {
            static std::error_code finalize(finalize_context<Engine>& ctx) { return {}; }
        };

        // kernel_base

        template <class Engine,
                  class Operation,
                  class OperationImpl,
                  typename HasInit = std::false_type,
                  typename HasFin  = std::false_type>
        struct kernel_base : kernel_initialize_base<Engine, Operation, OperationImpl, HasInit>,
                             kernel_compute_base<Engine, Operation, OperationImpl>,
                             kernel_finalize_base<Engine, Operation, OperationImpl, HasFin>
        {};

        // kernel_generator

        template <class Engine, class DeviceImpl, typename = engine_helpers::device::has_generator_t<DeviceImpl>>
        struct kernel_generator;

        // has a generator nested type

        template <class Engine, class DeviceImpl>
        struct kernel_generator<Engine, DeviceImpl, std::true_type>
        {
            template <class Operation>
            static void fill(DeviceImpl&, kernel_wrapper<Engine>* wrapper)
            {
                // TODO: assert (wrapper)

                using operation_impl_type = typename DeviceImpl::template generator<Operation>::type;

                *(wrapper) = kernel_wrapper(Engine::operations_list::template index<Operation>(),
                                            &kernel_base<Engine, Operation, operation_impl_type>::initialize,
                                            &kernel_base<Engine, Operation, operation_impl_type>::compute,
                                            &kernel_base<Engine, Operation, operation_impl_type>::finalize);
            }
        };
    }   // namespace _impl::kernel

    namespace _impl::device
    {
        template <class Engine>
        struct device_wrapper final
        {
            //            typedef std::error_code (*initialize_ptr_type)(initialize_context<Engine>&);

            template <class DeviceImpl>
            device_wrapper(DeviceImpl&& impl);

            ~device_wrapper();

            template <class DeviceImpl>
            DeviceImpl& as();

            template <class DeviceImpl>
            const DeviceImpl& as() const;

        private:
            std::unique_ptr<void> _impl;
        };
    }   // namespace _impl::device

    template <class Engine>
    struct device_base_internal final : interface_unique::internal<device_base<Engine>>
    {
        using operation_id_type = typename Engine::operation_id_type;

        using device_helpers = detail::device_helpers<Engine>;
        using device_wrapper = _impl::device::device_wrapper<Engine>;
        using kernel_wrapper = _impl::kernel::kernel_wrapper<Engine>;

        template <class DeviceImpl>
        explicit device_base_internal(graph_base<Engine>& graph, DeviceImpl&& impl)
            : _graph(graph_base_internal<Engine>::cast(graph)), _wrapper(std::forward<DeviceImpl>(impl))
        {
            static_for<Engine::n_operations>([&](auto i) {
                if constexpr (i >= detail::engine_helpers::operation::get_predefined_types_list_t::size())
                {
                    using operation_type = typename Engine::operations_list::template at_t<i>;
                    _impl::kernel::kernel_generator<Engine, DeviceImpl>::template fill<operation_type>(
                        _wrapper.template as<DeviceImpl>(), &_kernel_table[i]);
                }
            });
        }

        graph_base_internal<Engine>* graph() { return _graph; }
        const graph_base_internal<Engine>* graph() const { return _graph; }

        const kernel_wrapper& get_operation(operation_id_type id) const { return _kernel_table[id]; }

        template <class DataType>
        value_handle<Engine> allocate_value()
        {
            return {};
        }

        template <class SubType>
        void construct_value(value_handle<Engine>& handle)
        {}

        template <class SubType>
        void destroy_value(value_handle<Engine>& handle)
        {}

        void deallocate_value(value_handle<Engine>& handle) {}

    private:
        graph_base_internal<Engine>* _graph;
        device_wrapper _wrapper;
        std::array<kernel_wrapper, Engine::n_operations> _kernel_table;
    };

    //
    // value_base_internal
    //

    template <class Engine, class DataType>
    struct value_base_internal final : interface_inline::internal<value_base<Engine, DataType>>
    {
        explicit value_base_internal(device_base<Engine>& device) : _device(device_base_internal<Engine>::cast(device))
        {}

        value_base_internal(device_base<Engine>& device, const value_handle<Engine>& handle)
            : _device(device_base_internal<Engine>::cast(device)), _handle(handle)
        {}

        ~value_base_internal()
        {
            if (_handle.subtype_idx != value_handle<Engine>::None)
            {
                using subtypes_list_t = engine_helpers::data_type::get_subtypes_t<DataType>;

                index_visit<subtypes_list_t::size()>(
                    [this](auto idx) {
                        using I = decltype(idx);

                        using subtype_t = typename subtypes_list_t::template at_t<I::value>;

                        _device->template destroy_value<subtype_t>(_handle);
                    },
                    _handle.subtype_idx);
                _device->deallocate_value(_handle);
            }
        }

        device_base_internal<Engine>* device() { return _device; }
        const device_base_internal<Engine>* device() const { return _device; }

        value_handle<Engine> handle() const { return _handle; }

    private:
        device_base_internal<Engine>* _device;
        value_handle<Engine> _handle;
    };

    //
    // pipeline_base_internal
    //

    template <class Engine, class Signature>
    struct pipeline_base_internal final : interface_unique::internal<pipeline_base<Engine, Signature>>
    {
        using operation_id_type = typename Engine::operation_id_type;

        struct operation_data
        {
            operation_id_type operation_id;
            std::vector<std::size_t> input_nodes;
            std::size_t arg_i;

            operation_data(operation_id_type op_id,
                           std::vector<std::size_t>&& in_nodes,
                           std::size_t arg_i = std::size_t(-1))
                : operation_id(op_id), input_nodes(std::move(in_nodes)), arg_i(arg_i)
            {}
        };

        explicit pipeline_base_internal(device_base<Engine>& device)
            : _device(device_base_internal<Engine>::cast(device))
        {}

        device_base_internal<Engine>* device() { return _device; }
        const device_base_internal<Engine>* device() const { return _device; }

        array_view<operation_data> operations() const { return _operations; }
        void operations(std::vector<operation_data>&& ops) { _operations = std::move(ops); }

    private:
        device_base_internal<Engine>* _device;
        std::vector<operation_data> _operations;
    };
}   // namespace dg::detail
