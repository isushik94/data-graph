
#ifndef __DATA_GRAPH__ENGINE__MAIN_IPP__
#define __DATA_GRAPH__ENGINE__MAIN_IPP__

#include "./main.hpp"   // for IDEs

namespace dg
{
    // static constexpr bool data_type_is_allowed();

    template <class DataTypesList, class OperationsList, class Allocator>
    template <class DataType>
    inline constexpr bool engine<DataTypesList, OperationsList, Allocator>::data_type_is_allowed()
    {
        return engine::data_types_list::template contains<DataType>();
    }

    // static constexpr bool operation_is_allowed();

    template <class DataTypesList, class OperationsList, class Allocator>
    template <class Operation>
    inline constexpr bool engine<DataTypesList, OperationsList, Allocator>::operation_is_allowed()
    {
        return engine::operations_list::template contains<Operation>();
    }

    // static constexpr bool device_is_allowed();

    template <class DataTypesList, class OperationsList, class Allocator>
    template <class Device>
    inline constexpr bool engine<DataTypesList, OperationsList, Allocator>::device_is_allowed()
    {
        return detail::engine_integrity::is_valid_device_v<engine, Device>;
    }
}   // namespace dg

#endif
