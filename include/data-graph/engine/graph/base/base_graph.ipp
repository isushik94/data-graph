
namespace dg::detail
{
    namespace _impl
    {
        template <class Engine>
        using graph_interface_base = interface_unique::base<graph_base_internal<Engine>>;
    }

    // graph_base

    template <class Engine>
    graph_base<Engine>::graph_base(typename Engine::allocator_type alloc)
        : _impl::graph_interface_base<Engine>(std::in_place, alloc)
    {}

    template <class Engine>
    graph_base<Engine>::~graph_base()
    {}

    template <class Engine>
    graph_base<Engine>::graph_base(graph_base&& o, typename Engine::allocator_type alloc)
    {}

    template <class Engine>
    graph_base<Engine>::graph_base(graph_base&& o) noexcept : _impl::graph_interface_base<Engine>(std::move(o))
    {}

    template <class Engine>
    graph_base<Engine>& graph_base<Engine>::operator=(graph_base&& o) noexcept
    {
        _impl::graph_interface_base<Engine>::operator=(std::move(o));
        return *this;
    }

    // reset

    template <class Engine>
    inline void graph_base<Engine>::reset()
    {
        // _self = {};
    }

    // empty

    template <class Engine>
    inline bool graph_base<Engine>::empty() const
    {
        return true;
    }

    // place_identity

    template <class Engine>
    template <class DataType>
    inline void graph_base<Engine>::place_identity(node_type<DataType>& out_node)
    {
        auto iter     = this->self()->template append_identity_node<DataType>(true);
        auto internal = node_base_internal<Engine, DataType>::cast(out_node);
        internal->node_iter(iter);
    }

    // place_identity

    template <class Engine>
    template <class DataType, typename ArgNode>
    inline void graph_base<Engine>::place_identity(node_type<DataType>& out_node, ArgNode&& node)
    {
        auto iter     = this->self()->template append_identity_node<DataType>();
        auto internal = node_base_internal<Engine, DataType>::cast(out_node);
        internal->node_iter(iter);

        this->self()->template put_inputs<types_list<std::decay_t<DataType>>, ArgNode>(internal->node_iter(),
                                                                                       std::forward<ArgNode>(node));
    }

    // place_operation

    template <class Engine>
    template <class Operation, class OutDataType, typename... NodeTypes>
    inline void graph_base<Engine>::place_operation(node_type<OutDataType>& out_node, NodeTypes&&... nodes)
    {
        using input_types = typename Engine::template operation_get_input_types<Operation>;

        auto iter     = this->self()->template append_node<Operation, OutDataType>();
        auto internal = node_base_internal<Engine, OutDataType>::cast(out_node);
        internal->node_iter(iter);

        this->self()->template put_inputs<input_types, NodeTypes...>(internal->node_iter(),
                                                                     std::forward<NodeTypes>(nodes)...);
    }
}   // namespace dg::detail
