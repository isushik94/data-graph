
#ifndef __DATA_GRAPH__DETAIL__GRAPH__BASE_HPP__
#define __DATA_GRAPH__DETAIL__GRAPH__BASE_HPP__

#include <list>
#include <memory>
#include <string_view>

#include <data-graph/interface.hpp>

#include <data-graph/detail/engine/base.hpp>
#include <data-graph/detail/engine/helpers.hpp>

#include <data-graph/detail/graph/helpers.hpp>

namespace dg::detail
{
    /**
     * @class graph_base
     * @brief
     *
     * @tparam Engine
     */

    template <class Engine>
    struct graph_base_internal;

    template <class Engine>
    struct graph_base : interface_unique::base<graph_base_internal<Engine>>
    {
        static_assert(engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        using engine_type   = Engine;
        using graph_helpers = detail::graph_helpers<engine_type>;

        template <class DataType>
        using node_type = node_base<Engine, DataType>;

    protected:
        graph_base(typename Engine::allocator_type alloc);
        ~graph_base();

        graph_base(graph_base&& o, typename Engine::allocator_type alloc);
        graph_base(graph_base&& o) noexcept;
        graph_base& operator=(graph_base&& o) noexcept;

        graph_base(const graph_base& o) = delete;
        graph_base& operator=(const graph_base& o) = delete;

        inline void reset();

        inline bool empty() const;

        template <class DataType>
        inline void place_identity(node_type<DataType>& out_node);

        template <class DataType, typename ArgNode>
        inline void place_identity(node_type<DataType>& out_node, ArgNode&& node);

        template <class Operation, class OutDataType, typename... NodeTypes>
        inline void place_operation(node_type<OutDataType>& out_node, NodeTypes&&... nodes);
    };

    /**
     * @class node_base
     * @brief
     *
     * @tparam Engine
     * @tparam DataType
     */

    template <class Engine, class DataType>
    struct node_base_internal;

    template <class Engine, class DataType>
    struct node_base : interface_inline::base<node_base_internal<Engine, DataType>>
    {
        static_assert(engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        static_assert(Engine::template data_type_is_allowed<std::decay_t<DataType>>(),
                      "Please make sure if the type, used for the node class, is registered in the engine.");

        static_assert(std::is_same_v<DataType, std::decay_t<DataType>>,
                      "Please use a data type without reference or pointer qualifiers.");

        friend struct graph_base<Engine>;
        friend struct device_base<Engine>;

        using data_type     = DataType;
        using engine_type   = Engine;
        using graph_helpers = detail::graph_helpers<engine_type>;

    protected:
        node_base();
        node_base(graph_base<Engine>& parent);

        node_base(const node_base& o);
        node_base(node_base&& o) noexcept;

        ~node_base();

        node_base& operator=(const node_base& o);
        node_base& operator=(node_base&& o) noexcept;

        template <class AnotherDataType>
        static inline constexpr bool is();

        std::string_view name() const;

        inline bool operator==(const node_base& o) const;
        inline bool operator!=(const node_base& o) const;

        template <class AnotherType>
        inline bool operator==(const node_base<Engine, AnotherType>& o) const;
        template <class AnotherType>
        inline bool operator!=(const node_base<Engine, AnotherType>& o) const;
    };

}   // namespace dg::detail

#include "./_impl/base_common.ipp"
#include "./_impl/base_graph.ipp"
#include "./_impl/base_node.ipp"

#endif
