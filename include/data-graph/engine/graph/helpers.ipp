
namespace dg::detail
{
    //
    // graph_helpers
    //

    namespace _impl::graph
    {
        // has_field_data_type

        template <class T>
        struct has_field_data_type
        {
        private:
            template <typename C>
            static std::true_type check(typename C::data_type*);
            template <typename C>
            static std::false_type check(...);

        public:
            using type = decltype(check<T>(nullptr));

            static constexpr bool value = std::is_same_v<type, std::true_type>;
        };

        template <class T>
        using has_field_data_type_t = typename has_field_data_type<T>::type;

        template <class T>
        inline constexpr bool has_field_data_type_v = has_field_data_type<T>::value;
    }   // namespace _impl::graph

    // is_node

    namespace _impl::graph
    {
        template <class Engine, class Node, typename = has_field_data_type_t<Node>>
        struct is_node;

        template <class Engine, class Node>
        struct is_node<Engine, Node, std::false_type> : public std::false_type
        {};

        template <class Engine, class Node>
        struct is_node<Engine, Node, std::true_type>
            : public std::disjunction<std::is_same<Node, detail::node_base<Engine, typename Node::data_type>>,
                                      std::is_base_of<detail::node_base<Engine, typename Node::data_type>, Node>>::type
        {};
    }   // namespace _impl::graph

    template <class Engine>
    template <class Node>
    struct graph_helpers<Engine>::is_node : public _impl::graph::is_node<Engine, Node>
    {};

    // get_node_type

    namespace _impl::graph
    {
        template <class Engine, class Node, typename = typename graph_helpers<Engine>::template is_node<Node>::type>
        struct get_node_data_type;

        template <class Engine, class Node>
        struct get_node_data_type<Engine, Node, std::false_type>
        {
            using type = void;
        };

        template <class Engine, class Node>
        struct get_node_data_type<Engine, Node, std::true_type>
        {
            using type = typename Node::data_type;
        };
    }   // namespace _impl::graph

    template <class Engine>
    template <class Node>
    struct graph_helpers<Engine>::get_node_data_type : public _impl::graph::get_node_data_type<Engine, Node>
    {};
}   // namespace dg::detail
