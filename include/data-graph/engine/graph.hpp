
#ifndef __DATA_GRAPH__ENGINE__GRAPH_HPP__
#define __DATA_GRAPH__ENGINE__GRAPH_HPP__

#ifndef __DATA_GRAPH__ENGINE_HPP__
#    error "Please use `#include <data-graph/engine.hpp>` instead. This header is not supposed to be used directly."
#endif

#include <data-graph/engine/main/helpers.hpp>

#include <data-graph/engine/graph/base.hpp>
#include <data-graph/engine/graph/helpers.hpp>

namespace dg
{
    /**
     * @class graph
     * @brief
     *
     */

    template <class Engine>
    struct graph final : detail::graph_base<Engine>
    {
        static_assert(detail::engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        using engine_type    = Engine;
        using allocator_type = typename engine_type::allocator_type;
        using base_type      = detail::graph_base<Engine>;

        template <class DataType>
        struct node;

        //

        graph();
        graph(allocator_type alloc);

        ~graph() = default;

        graph(graph&& o) noexcept = default;
        graph& operator=(graph&& o) noexcept = default;

        graph(const graph& o) = delete;
        graph& operator=(const graph& o) = delete;

        /**
         * @brief
         *
         * @tparam DataType One of the registered data types in the engine in use.
         * @return A new identity node suitable for broadcasting data in runtime.
         */

        template <class DataType>
        [[nodiscard]] node<DataType> identity();

        /**
         * @brief
         *
         * @tparam DataType One of the registered data types in the engine in use.
         * @tparam ArgNode Type of the argument (`node<Type>` or one of the types specified in `decl_data_type` helper).
         *
         * @param node_in Another node or a constant as a default value.
         * @return A new identity node which has a default value.
         */

        template <class DataType, typename ArgNode>
        [[nodiscard]] node<DataType> identity(ArgNode&& node_in);

        /**
         * @brief
         *
         * @tparam Operation One of the registered operations in the engine in use.
         * @tparam NodeTypes A variadic list of types of provided arguments.
         *
         * @param nodes A variadic list of arguments which can be graph nodes or constants.
         * @return A new node which hold the result of the operation.
         */

        template <class Operation, typename... NodeTypes>
        [[nodiscard]] auto operation(NodeTypes&&... nodes);

        /**
         * @method auto cond(const node<bool>& pred, TrueNodeType&& true_node, FalseNodeType&& false_node)
         * @brief
         *
         * @tparam TrueNodeType
         * @tparam FalseNodeType
         *
         * @param pred
         * @param true_node
         * @param false_node
         * @return
         */

        template <class TrueNodeType, class FalseNodeType>
        auto cond(const node<bool>& pred, TrueNodeType&& true_node, FalseNodeType&& false_node);

    private:
        template <class DataType, class ArgNode, std::size_t I = 0>
        static constexpr bool _validate_argument();
    };

    /**
     * @class node
     * @brief
     *
     * @tparam DataType
     */

    template <class Engine>
    template <class DataType>
    struct graph<Engine>::node final : detail::node_base<Engine, DataType>
    {
        using engine_type = Engine;

        static_assert(engine_type::template data_type_is_allowed<DataType>(),
                      "Please make sure if the type, used for the node class, is registered in the engine.");

        static_assert(std::is_same_v<DataType, std::decay_t<DataType>>,
                      "Please use a type without reference or pointer qualifiers and so on.");

        friend struct graph<Engine>;

        using base_type = detail::node_base<engine_type, DataType>;

        node()  = default;
        ~node() = default;

        node(const node& o);

    private:
        explicit node(graph& parent);
    };
}   // namespace dg

#include "./graph.ipp"

#endif
