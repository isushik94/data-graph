
#ifndef __DATA_GRAPH__ENGINE__MAIN__INTEGRITY_IPP__
#define __DATA_GRAPH__ENGINE__MAIN__INTEGRITY_IPP__

#include "./integrity.hpp"   // for IDEs

namespace dg::detail::engine_integrity
{
    // struct is_valid_data_type

    template <class DataType>
    struct is_valid_data_type : engine_helpers::sfinae::has_nested__types_t<DataType>::type
    {};

    template <>
    struct is_valid_data_type<bool> : std::true_type
    {};

    template <typename... Types>
    struct is_valid_data_type<declarations::data_type<Types...>> : std::true_type
    {};

    // struct is_valid_data_types

    namespace _impl::data_type
    {
        template <class DataTypesList>
        struct is_valid_data_types;

        template <class... DataTypes>
        struct is_valid_data_types<types_list<DataTypes...>> : std::conjunction<is_valid_data_type<DataTypes>...>::type
        {};
    }   // namespace _impl::data_type

    template <class DataTypesList>
    struct is_valid_data_types
        : _impl::data_type::is_valid_data_types<engine_helpers::data_type::get_types_list_t<DataTypesList>>::type
    {};

    // struct is_valid_operation

    template <class DataTypesList, class Operation>
    struct is_valid_operation
        : std::conjunction<typename engine_helpers::data_type::get_types_list_t<DataTypesList>::template contains_t<
                               engine_helpers::operation::get_output_type_t<Operation>>,
                           typename engine_helpers::data_type::get_types_list_t<DataTypesList>::template is_sublist_t<
                               engine_helpers::operation::get_input_types_t<Operation>>>::type
    {};

    template <class DataTypesList>
    struct is_valid_operation<DataTypesList, engine_helpers::operation::identity> : std::true_type
    {};

    template <class DataTypesList>
    struct is_valid_operation<DataTypesList, engine_helpers::operation::condition> : std::true_type
    {};

    // struct is_valid_operations

    namespace _impl::operation
    {
        template <class DataTypesList, class OperationsList>
        struct is_valid_operations;

        template <class DataTypesList, class... Operations>
        struct is_valid_operations<DataTypesList, types_list<Operations...>>
            : std::conjunction<is_valid_operation<DataTypesList, Operations>...>::type
        {};
    }   // namespace _impl::operation

    template <class DataTypesList, class OperationsList>
    struct is_valid_operations
        : _impl::operation::is_valid_operations<DataTypesList,
                                                engine_helpers::operation::get_types_list_t<OperationsList>>::type
    {};

    // struct is_valid_engine

    template <class DataTypesList, class OperationsList>
    struct is_valid_engine
        : std::conjunction<is_valid_data_types<DataTypesList>, is_valid_operations<DataTypesList, OperationsList>>::type
    {};

    // struct is_valid_device

    template <class Engine, class Device>
    struct is_valid_device;

    template <class DataTypesList, class OperationsList, class Device>
    struct is_valid_device<::dg::engine<DataTypesList, OperationsList>, Device>
        : engine_helpers::device::is_kernels_provider_t<Device>
    {};
}   // namespace dg::detail::engine_integrity

#endif
