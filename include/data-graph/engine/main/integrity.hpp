
#ifndef __DATA_GRAPH__ENGINE__MAIN__INTEGRITY_HPP__
#define __DATA_GRAPH__ENGINE__MAIN__INTEGRITY_HPP__

#ifndef __DATA_GRAPH__ENGINE__MAIN_HPP__
#    error "Please use `#include <data-graph/engine.hpp>` instead. This header is not supposed to be used directly."
#endif

#include <data-graph/type_traits.hpp>

#include "./helpers.hpp"

namespace dg::detail::engine_integrity
{
    /**
     * @class is_valid_data_type
     * @brief
     *
     * @tparam DataType
     *
     * @member value
     */

    template <class DataType>
    struct is_valid_data_type;

    template <class DataType>
    static constexpr auto is_valid_data_type_v = is_valid_data_type<DataType>::value;

    /**
     * @class is_valid_data_types
     * @brief
     *
     * @tparam DataTypesList
     *
     * @member value
     */

    template <class DataTypesList>
    struct is_valid_data_types;

    template <class DataTypesList>
    static constexpr auto is_valid_data_types_v = is_valid_data_types<DataTypesList>::value;

    /**
     * @class is_valid_operation
     * @brief
     *
     * @tparam DataTypesList
     * @tparam Operation
     *
     * @member value
     */

    template <class DataTypesList, class Operation>
    struct is_valid_operation;

    template <class DataTypesList, class Operation>
    static constexpr auto is_valid_operation_v = is_valid_operation<DataTypesList, Operation>::value;

    /**
     * @class is_valid_operations
     * @brief
     *
     * @tparam DataTypesList
     * @tparam OperationsList
     *
     * @member value
     */

    template <class DataTypesList, class OperationsList>
    struct is_valid_operations;

    template <class DataTypesList, class OperationsList>
    static constexpr auto is_valid_operations_v = is_valid_operations<DataTypesList, OperationsList>::value;

    /**
     * @class is_valid_engine
     * @brief
     *
     * @tparam DataTypesList
     * @tparam OperationsList
     *
     * @member value
     */

    template <class DataTypesList, class OperationsList>
    struct is_valid_engine;

    template <class DataTypesList, class OperationsList>
    static constexpr auto is_valid_engine_v = is_valid_engine<DataTypesList, OperationsList>::value;

    /**
     * @class is_valid_device
     * @brief
     *
     * @tparam OperationsList
     * @tparam Device
     *
     * @member value
     */

    template <class Engine, class Device>
    struct is_valid_device;

    template <class Engine, class Device>
    static constexpr auto is_valid_device_v = is_valid_device<Engine, Device>::value;
}   // namespace dg::detail::engine_integrity

#include "./integrity.ipp"

#endif
