
#ifndef __DATA_GRAPH__ENGINE__MAIN__HELPERS_HPP__
#define __DATA_GRAPH__ENGINE__MAIN__HELPERS_HPP__

#ifndef __DATA_GRAPH__ENGINE__MAIN_HPP__
#    error "Please use `#include <data-graph/engine.hpp>` instead. This header is not supposed to be used directly."
#endif

#include <data-graph/string.hpp>
#include <data-graph/type_traits.hpp>

namespace dg::detail::engine_helpers
{
    /**
     * @class is_engine
     * @brief
     *
     * @tparam Engine
     *
     * @member value
     */

    template <class Engine>
    struct is_engine;

    template <class Engine>
    inline constexpr auto is_engine_v = is_engine<Engine>::value;

    namespace data_type
    {
        /**
         * @class get_predefined_types_list
         * @brief
         *
         * @member type
         */

        struct get_predefined_types_list;

        /**
         * @class get_types_list
         * @brief
         *
         * @tparam DataTypesList
         *
         * @member type
         */

        template <class DataTypesList>
        struct get_types_list;

        template <class DataTypesList>
        using get_types_list_t = typename get_types_list<DataTypesList>::type;

        /**
         * @class get_names_table
         * @brief
         *
         * @tparam DataTypesList
         *
         * @member value
         */

        template <class DataTypesList>
        struct get_names_table;

        template <class DataTypesList>
        inline constexpr auto get_names_table_v = get_names_table<DataTypesList>::value;

        /**
         * @class get_subtypes
         * @brief
         *
         * @tparam Operation
         *
         * @member type
         */

        template <class DataType>
        struct get_subtypes;

        template <class DataType>
        using get_subtypes_t = typename get_subtypes<DataType>::type;

        /**
         * @class get_max_size
         * @brief
         *
         * @tparam DataType
         *
         * @member value
         */

        template <class DataType>
        struct get_max_size;

        template <class DataType>
        inline constexpr auto get_max_size_v = get_max_size<DataType>::value;

        /**
         * @class get_max_n_subtypes
         * @brief
         *
         * @tparam DataTypesList
         *
         * @member value
         */

        template <class DataTypesList>
        struct get_max_n_subtypes;

        template <class DataTypesList>
        inline constexpr auto get_max_n_subtypes_v = get_max_n_subtypes<DataTypesList>::value;

        /**
         * @class get_id
         * @brief
         *
         * @tparam DataTypesList
         * @tparam DataType
         *
         * @member value
         */

        template <class DataTypesList, class DataType>
        struct get_id;

        template <class DataTypesList, class DataType>
        inline constexpr auto get_id_v = get_id<DataTypesList, DataType>::value;
    }   // namespace data_type

    namespace operation
    {
        struct identity final
        {
            static constexpr std::string_view name = "identity";
        };

        struct condition final
        {
            static constexpr std::string_view name = "condition";
        };

        /**
         * @class get_predefined_types_list
         * @brief
         *
         * @member type
         */

        struct get_predefined_types_list;

        /**
         * @class get_types_list
         * @brief
         *
         * @tparam OperationsList
         *
         * @member type
         */

        template <class OperationsList>
        struct get_types_list;

        template <class OperationsList>
        using get_types_list_t = typename get_types_list<OperationsList>::type;

        /**
         * @class get_names_table
         * @brief
         *
         * @tparam OperationsList
         *
         * @member value
         */

        template <class OperationsList>
        struct get_names_table;

        template <class OperationsList>
        inline constexpr auto get_names_table_v = get_names_table<OperationsList>::value;

        /**
         * @class get_input_types
         * @brief
         *
         * @tparam Operation
         *
         * @member type
         */

        template <class Operation>
        struct get_input_types;

        template <class Operation>
        using get_input_types_t = typename get_input_types<Operation>::type;

        /**
         * @class get_output_type
         * @brief
         *
         * @tparam Operation
         *
         * @member type
         */

        template <class Operation>
        struct get_output_type;

        template <class Operation>
        using get_output_type_t = typename get_output_type<Operation>::type;

        /**
         * @class get_max_n_arguments
         * @brief
         *
         * @tparam OperationsList
         *
         * @member value
         */

        template <class OperationsList>
        struct get_max_n_arguments;

        template <class OperationsList>
        inline constexpr auto get_max_n_arguments_v = get_max_n_arguments<OperationsList>::value;

        /**
         * @class get_id
         * @brief
         *
         * @tparam OperationsList
         * @tparam Operation
         *
         * @member value
         */

        template <class OperationsList, class Operation>
        struct get_id;

        template <class OperationsList, class Operation>
        inline constexpr auto get_id_v = get_id<OperationsList, Operation>::value;
    }   // namespace operation

    namespace device
    {
        /**
         * @class has_generator
         * @brief
         *
         * @tparam Device
         *
         * @member value
         */

        template <class Device>
        struct has_generator;

        template <class Device>
        using has_generator_t = typename has_generator<Device>::type;

        template <class Device>
        inline constexpr auto has_generator_v = has_generator<Device>::value;

        /**
         * @class is_kernels_provider
         * @brief
         *
         * @tparam Device
         *
         * @member value
         */

        template <class Device>
        struct is_kernels_provider;

        template <class Device>
        using is_kernels_provider_t = typename is_kernels_provider<Device>::type;

        template <class Device>
        inline constexpr auto is_kernels_provider_v = is_kernels_provider<Device>::value;
    }   // namespace device

    namespace sfinae
    {
        /**
         * @class has_member__name
         * @tparam T
         */

        template <class T>
        struct has_member__name;

        template <class T>
        using has_member__name_t = typename has_member__name<T>::type;

        template <class T>
        inline constexpr bool has_member__name_v = has_member__name<T>::value;

        /**
         * @class has_nested__types
         * @tparam T
         */

        template <class T>
        struct has_nested__types;

        template <class T>
        using has_nested__types_t = typename has_nested__types<T>::type;

        template <class T>
        inline constexpr bool has_nested__types_v = has_nested__types<T>::value;

        /**
         * @class has_nested__input_types
         * @tparam T
         */

        template <class T>
        struct has_nested__input_types;

        template <class T>
        using has_nested__input_types_t = typename has_nested__input_types<T>::type;

        template <class T>
        inline constexpr bool has_nested__input_types_v = has_nested__input_types<T>::value;

        /**
         * @class has_nested__output_type
         * @tparam T
         */

        template <class T>
        struct has_nested__output_type;

        template <class T>
        using has_nested__output_type_t = typename has_nested__output_type<T>::type;

        template <class T>
        inline constexpr bool has_nested__output_type_v = has_nested__output_type<T>::value;

        /**
         * @class has_nested__generator
         * @tparam T
         */

        template <class T>
        struct has_nested__generator;

        template <class T>
        using has_nested__generator_t = typename has_nested__generator<T>::type;

        template <class T>
        inline constexpr bool has_nested__generator_v = has_nested__generator<T>::value;
    }   // namespace sfinae
}   // namespace dg::detail::engine_helpers

#include "./helpers.ipp"

#include "./helpers/data_type.ipp"

#include "./helpers/operation.ipp"

#include "./helpers/device.ipp"

#include "./helpers/sfinae.ipp"

#endif
