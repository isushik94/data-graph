
#ifndef __DATA_GRAPH__ENGINE__MAIN__HELPERS__SFINAE_IPP__
#define __DATA_GRAPH__ENGINE__MAIN__HELPERS__SFINAE_IPP__

#include "../helpers.hpp"

namespace dg::detail::engine_helpers::sfinae
{
    // struct has_member__name

    namespace _impl
    {
        template <class T>
        struct has_member__name
        {
        private:
            template <typename C>
            static std::true_type check(decltype(&C::name));
            template <typename C>
            static std::false_type check(...);

        public:
            using type = decltype(check<T>(nullptr));

            static constexpr bool value = std::is_same_v<type, std::true_type>;
        };
    }   // namespace _impl

    template <class T>
    struct has_member__name : public _impl::has_member__name<T>::type
    {};

    // struct has_nested__types

    namespace _impl
    {
        template <class T>
        struct has_nested__types
        {
        private:
            template <typename C>
            static std::true_type check(typename C::types*);
            template <typename C>
            static std::false_type check(...);

        public:
            using type = decltype(check<T>(nullptr));

            static constexpr bool value = std::is_same_v<type, std::true_type>;
        };
    }   // namespace _impl

    template <class T>
    struct has_nested__types : _impl::has_nested__types<T>::type
    {};

    // struct has_nested__input_types

    namespace _impl
    {
        template <class T>
        struct has_nested__input_types
        {
        private:
            template <typename C>
            static std::true_type check(typename C::input_types*);
            template <typename C>
            static std::false_type check(...);

        public:
            using type = decltype(check<T>(nullptr));

            static constexpr bool value = std::is_same_v<type, std::true_type>;
        };
    }   // namespace _impl

    template <class T>
    struct has_nested__input_types : _impl::has_nested__input_types<T>::type
    {};

    // struct has_nested__types

    namespace _impl
    {
        template <class T>
        struct has_nested__output_type
        {
        private:
            template <typename C>
            static std::true_type check(typename C::output_type*);
            template <typename C>
            static std::false_type check(...);

        public:
            using type = decltype(check<T>(nullptr));

            static constexpr bool value = std::is_same_v<type, std::true_type>;
        };
    }   // namespace _impl

    template <class T>
    struct has_nested__output_type : _impl::has_nested__output_type<T>::type
    {};

    // struct has_nested__generator

    namespace _impl
    {
        template <class T>
        struct has_nested__generator
        {
        private:
            template <typename C>
            static std::true_type check(typename C::generator*);
            template <typename C>
            static std::false_type check(...);

        public:
            using type = decltype(check<T>(nullptr));

            static constexpr bool value = std::is_same_v<type, std::true_type>;
        };
    }   // namespace _impl

    template <class T>
    struct has_nested__generator : _impl::has_nested__generator<T>::type
    {};
}   // namespace dg::detail::engine_helpers::sfinae

#endif
