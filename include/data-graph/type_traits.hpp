#ifndef __TYPE_TRAITS_HPP__
#define __TYPE_TRAITS_HPP__

#include <tuple>
#include <variant>

//
// types list
//

template <typename T, class TypesList>
struct types_list_contains;

template <class TypesSubList, class TypesList>
struct types_list_is_sublist;

template <class TypesList1, class TypesList2>
struct types_list_is_equal_set;

template <typename T, class TypesList>
struct types_list_index;

template <class TypesList, class AnotherTypesList>
struct types_list_starts_with;

template <template <typename> class F, class TypesList>
struct types_list_map;

template <class TypesList, std::size_t Start = 0, std::size_t End = TypesList::size()>
struct types_list_slice;

template <typename... Ts>
struct types_list
{
    static constexpr std::size_t size() { return sizeof...(Ts); }

    template <std::size_t idx>
    struct at
    {
        using type = typename std::tuple_element<idx, std::tuple<Ts...>>::type;
    };

    template <std::size_t idx>
    using at_t = typename at<idx>::type;

    template <typename Another>
    struct concat;

    template <typename... Us>
    struct concat<types_list<Us...>>
    {
        using type = types_list<Ts..., Us...>;
    };

    template <typename Another>
    using concat_t = typename concat<Another>::type;

    template <std::size_t Start = 0, std::size_t End = types_list::size()>
    struct slice
    {
        using type = typename types_list_slice<types_list, Start, End>::type;
    };

    template <std::size_t Start = 0, std::size_t End = types_list::size()>
    using slice_t = typename slice<Start, End>::type;

    template <typename T>
    static constexpr std::size_t index()
    {
        return types_list_index<T, types_list>::value;
    }

    template <typename T>
    static constexpr bool contains()
    {
        return types_list_contains<T, types_list>::value;
    }

    template <typename T>
    using contains_t = std::bool_constant<contains<T>()>;

    template <class SubList>
    static constexpr bool is_sublist()
    {
        return types_list_is_sublist<SubList, types_list>::value;
    }

    template <class SubList>
    using is_sublist_t = std::bool_constant<is_sublist<SubList>()>;

    template <class AnotherList>
    static constexpr bool is_equal_set()
    {
        return types_list_is_equal_set<types_list, AnotherList>::value;
    }

    template <class AnotherList>
    using is_equal_set_t = std::bool_constant<is_equal_set<AnotherList>()>;

    template <typename Another>
    static constexpr bool starts_with()
    {
        return types_list_starts_with<types_list, Another>::value;
    }

    template <typename Another>
    using starts_with_t = std::bool_constant<starts_with<Another>()>;

    struct unique
    {
        // TODO : implement removing duplicats
        using type = types_list;
    };

    using unique_t = typename unique::type;

    //

    template <template <typename> class F>
    struct map
    {
        using type = typename types_list_map<F, types_list>::type;
    };

    template <template <typename> class F>
    using map_t = typename map<F>::type;
};

template <class TypeList>
struct as_variant;

template <typename... Ts>
struct as_variant<types_list<Ts...>>
{
    using type = std::variant<std::decay_t<Ts>...>;
};

template <class TypeList>
using as_variant_t = typename as_variant<TypeList>::type;

template <class TypeList>
struct as_tuple;

template <typename... Ts>
struct as_tuple<types_list<Ts...>>
{
    using type = std::tuple<Ts...>;
};

// types_list_append

template <class List, typename T>
struct types_list_append;

template <typename... Ts, typename T>
struct types_list_append<types_list<Ts...>, T>
{
    typedef types_list<Ts..., T> type;
};

template <class L, typename T>
using types_list_append_t = typename types_list_append<L, T>::type;

// types_list_slice

namespace _impl
{
    template <class TypesList, std::size_t Start>
    struct types_list_slice
    {
        template <class Indices>
        struct enumerate;

        template <std::size_t... Is>
        struct enumerate<std::index_sequence<Is...>>
        {
            using type = types_list<typename TypesList::template at_t<Start + Is>...>;
        };
    };
}   // namespace _impl

template <class TypesList, std::size_t Start, std::size_t End>
struct types_list_slice
{
    static_assert(Start < End, "Start index must be less than End index.");
    static_assert(TypesList::size() >= End, "Wrong End index.");

    using type =
        typename _impl::types_list_slice<TypesList,
                                         Start>::template enumerate<std::make_index_sequence<End - Start>>::type;
};

// types_list_rank

template <int N>
struct types_list_rank : types_list_rank<N - 1>
{};

template <>
struct types_list_rank<0>
{};

// get

template <class List, std::size_t idx>
struct get;

template <class... Ts, std::size_t idx>
struct get<types_list<Ts...>, idx>
{
    using type = typename std::tuple_element<idx, std::tuple<Ts...>>::type;
};

template <class List, unsigned idx>
using get_t = typename get<List, idx>::type;

// types_list_contains

template <typename T, class TypesList>
struct types_list_contains;

template <typename T>
struct types_list_contains<T, types_list<>> : public std::false_type
{};

template <typename T, typename... REST>
struct types_list_contains<T, types_list<T, REST...>> : public std::true_type
{};

template <typename T, typename NOT_T, typename... REST>
struct types_list_contains<T, types_list<NOT_T, REST...>> : public types_list_contains<T, types_list<REST...>>
{};

// template <class TypesList, typename... Ts>
// struct types_list_contains<types_list<Ts...>, TypesList>
//     : public std::conjunction<types_list_contains<Ts, TypesList>...>::type
// {};

template <typename T, class TypesList>
inline constexpr bool types_list_contains_v = types_list_contains<T, TypesList>::value;

// types_list_is_sublist

template <class TypesList, typename... Ts>
struct types_list_is_sublist<types_list<Ts...>, TypesList>
    : public std::conjunction<types_list_contains<Ts, TypesList>...>::type
{};

template <typename TypesSubList, class TypesList>
inline constexpr bool types_list_is_sublist_v = types_list_is_sublist<TypesSubList, TypesList>::value;

// types_list_is_equal_set

template <class TypesList1, class TypesList2>
struct types_list_is_equal_set : public std::bool_constant<types_list_is_sublist_v<TypesList1, TypesList2> &&
                                                           types_list_is_sublist_v<TypesList2, TypesList1>>
{};

template <class TypesList1, class TypesList2>
using types_list_is_equal_set_t = typename types_list_is_equal_set<TypesList1, TypesList2>::type;

template <class TypesList1, class TypesList2>
inline constexpr bool types_list_is_equal_set_v = types_list_is_equal_set<TypesList1, TypesList2>::value;

// types_list_index

template <typename T>
struct types_list_index<T, types_list<>>
{
    static inline constexpr const std::size_t value = std::size_t(-1);
};

template <typename T, typename... Rest>
struct types_list_index<T, types_list<T, Rest...>>
{
    static inline constexpr const std::size_t value = 0;
};

template <typename T, typename NotT, typename... Rest>
struct types_list_index<T, types_list<NotT, Rest...>>
{
    static inline constexpr const std::size_t value =
        std::plus<std::size_t>()(1, types_list_index<T, types_list<Rest...>>::value);
};

template <typename T, class TypesList>
inline constexpr const auto types_list_index_v = types_list_index<T, TypesList>::value;

// types_list_zip

namespace _details
{
    template <class TypesList>
    struct zip;

    template <typename... Types1>
    struct zip<types_list<Types1...>>
    {
        template <class TypesList>
        struct with;

        template <typename... Types2>
        struct with<types_list<Types2...>>
        {
            using type = types_list<types_list<Types1, Types2>...>;
        };
    };
}   // namespace _details

template <class TypesList1, class TypesList2>
struct types_list_zip : public _details::zip<TypesList1>::template with<TypesList2>
{};

template <class TypesList1, class TypesList2>
using types_list_zip_t = typename types_list_zip<TypesList1, TypesList2>::type;

// types_list_starts_with

namespace _details
{
    template <class TypesList,
              class AnotherTypesList,
              typename = std::bool_constant<(TypesList::size() >= AnotherTypesList::size())>>
    struct types_list_starts_with;

    template <class TypesList, class AnotherTypesList>
    struct types_list_starts_with<TypesList, AnotherTypesList, std::false_type>
    {
        using type = std::false_type;
    };

    template <class TypesList, class AnotherTypesList>
    struct types_list_starts_with<TypesList, AnotherTypesList, std::true_type>
    {
        template <class Indices>
        struct enumerate;

        template <std::size_t... Is>
        struct enumerate<std::index_sequence<Is...>>
        {
            using type = typename std::conjunction<std::is_same<typename TypesList::template at_t<Is>,
                                                                typename AnotherTypesList::template at_t<Is>>...>::type;
        };

        using type = typename enumerate<std::make_index_sequence<AnotherTypesList::size()>>::type;
    };
}   // namespace _details

template <class TypesList, class AnotherTypesList>
struct types_list_starts_with : public _details::types_list_starts_with<TypesList, AnotherTypesList>::type
{};

// int_type_detector

namespace _details
{
    template <typename Int8, typename Int16, typename Int32, typename Int64>
    struct int_type_detector_impl;

    template <>
    struct int_type_detector_impl<std::true_type, std::false_type, std::false_type, std::false_type>
    {
        using type = std::int8_t;
    };

    template <>
    struct int_type_detector_impl<std::true_type, std::true_type, std::false_type, std::false_type>
    {
        using type = std::int16_t;
    };

    template <>
    struct int_type_detector_impl<std::true_type, std::true_type, std::true_type, std::false_type>
    {
        using type = std::int32_t;
    };

    template <>
    struct int_type_detector_impl<std::true_type, std::true_type, std::true_type, std::true_type>
    {
        using type = std::int64_t;
    };
}   // namespace _details

template <std::intmax_t MaxV>
struct int_type_detector
{
    using type = typename _details::int_type_detector_impl<
        std::integral_constant<bool, (MaxV <= std::intmax_t(INT8_MAX))>,
        std::integral_constant<bool, (MaxV <= std::intmax_t(INT16_MAX))>,
        std::integral_constant<bool, (MaxV <= std::intmax_t(INT32_MAX))>,
        std::integral_constant<bool, (MaxV <= std::intmax_t(INT64_MAX))>>::type;
};

template <std::intmax_t MaxV>
using int_type_detector_t = typename int_type_detector<MaxV>::type;

// uint_type_detector

namespace _details
{
    template <typename Uint8, typename Uint16, typename Uint32, typename Uint64>
    struct uint_type_detector_impl;

    template <>
    struct uint_type_detector_impl<std::true_type, std::false_type, std::false_type, std::false_type>
    {
        using type = std::uint8_t;
    };

    template <>
    struct uint_type_detector_impl<std::true_type, std::true_type, std::false_type, std::false_type>
    {
        using type = std::uint16_t;
    };

    template <>
    struct uint_type_detector_impl<std::true_type, std::true_type, std::true_type, std::false_type>
    {
        using type = std::uint32_t;
    };

    template <>
    struct uint_type_detector_impl<std::true_type, std::true_type, std::true_type, std::true_type>
    {
        using type = std::uint64_t;
    };
}   // namespace _details

template <std::uintmax_t MaxV, bool WithResetV = false>
struct uint_type_detector
{
    using type = typename _details::uint_type_detector_impl<
        std::integral_constant<bool, ((WithResetV ? MaxV + 1 : MaxV) <= std::uintmax_t(UINT8_MAX))>,
        std::integral_constant<bool, ((WithResetV ? MaxV + 1 : MaxV) <= std::uintmax_t(UINT16_MAX))>,
        std::integral_constant<bool, ((WithResetV ? MaxV + 1 : MaxV) <= std::uintmax_t(UINT32_MAX))>,
        std::integral_constant<bool, ((WithResetV ? MaxV + 1 : MaxV) <= std::uintmax_t(UINT64_MAX))>>::type;
};

template <std::uintmax_t MaxV, bool WithResetV = false>
using uint_type_detector_t = typename uint_type_detector<MaxV, WithResetV>::type;

// template math

namespace _details
{
    template <template <auto, auto> class F, auto... Vs>
    struct reduce_impl;

    template <template <auto, auto> class F, auto V1, auto V2>
    struct reduce_impl<F, V1, V2>
    {
        static inline constexpr const auto value = F<V1, V2>::value;
    };

    template <template <auto, auto> class F, auto V1, auto V2, auto... Vs>
    struct reduce_impl<F, V1, V2, Vs...>
    {
        static inline constexpr const auto value = (reduce_impl<F, (reduce_impl<F, V1, V2>::value), Vs...>::value);
    };
}   // namespace _details

template <template <auto, auto> class F, auto... Vs>
struct reduce
{
    static inline constexpr const auto value = _details::reduce_impl<F, Vs...>::value;
};

template <template <auto, auto> class F, auto... Vs>
inline constexpr const auto reduce_v = reduce<F, Vs...>::value;

namespace reduce_ops
{
    // max

    namespace _details
    {
        template <auto V1, auto V2, typename>
        struct max_impl;

        template <auto V1, auto V2>
        struct max_impl<V1, V2, std::true_type>
        {
            static inline constexpr const auto value = V1;
        };

        template <auto V1, auto V2>
        struct max_impl<V1, V2, std::false_type>
        {
            static inline constexpr const auto value = V2;
        };
    }   // namespace _details

    template <auto V1, auto V2>
    struct max
    {
        static inline constexpr const auto value =
            _details::max_impl<V1, V2, std::integral_constant<bool, (V1 > V2)>>::value;
    };

    // min

    namespace _details
    {
        template <auto V1, auto V2, typename>
        struct min_impl;

        template <auto V1, auto V2>
        struct min_impl<V1, V2, std::true_type>
        {
            static inline constexpr const auto value = V1;
        };

        template <auto V1, auto V2>
        struct min_impl<V1, V2, std::false_type>
        {
            static inline constexpr const auto value = V2;
        };
    }   // namespace _details

    template <auto V1, auto V2>
    struct min
    {
        static inline constexpr const auto value =
            _details::min_impl<V1, V2, std::integral_constant<bool, (V1 < V2)>>::value;
    };

    // sum
    template <auto V1, auto V2>
    struct sum
    {
        static inline constexpr const auto value = (V1 + V2);
    };
}   // namespace reduce_ops

// types_list_map

template <template <typename> class F, class TypesList>
struct types_list_map;

template <template <typename> class F, typename... Types>
struct types_list_map<F, types_list<Types...>>
{
    using type = types_list<typename F<Types>::type...>;
};

template <template <typename> class F, class TypesList>
using types_list_map_t = typename types_list_map<F, TypesList>::type;

// static_for

namespace
{
    template <class Func, std::size_t... Is>
    constexpr void static_for_impl(Func&& f, std::index_sequence<Is...>)
    {
        (f(std::integral_constant<std::size_t, Is>{}), ...);
    }

    template <class Tuple, class Func, std::size_t... Is>
    constexpr void static_for_impl(Tuple&& t, Func&& f, std::index_sequence<Is...>)
    {
        (f(std::integral_constant<std::size_t, Is>{}, std::get<Is>(t)), ...);
    }
}   // namespace

template <std::size_t N, class Func>
constexpr void static_for(Func&& f)
{
    static_for_impl(std::forward<Func>(f), std::make_index_sequence<N>{});
}

template <class Func, class... T>
constexpr void static_for(std::tuple<T...>& t, Func&& f)
{
    static_for_impl(t, std::forward<Func>(f), std::make_index_sequence<sizeof...(T)>{});
}

// tuple_visit

namespace _impl
{
    template <size_t I>
    struct tuple_visit
    {
        template <typename Visitor, typename Tuple>
        static auto visit(Visitor&& vis, const Tuple& tup, size_t idx)
        {
            if (idx == I - 1)
                return vis(std::get<I - 1>(tup));
            else
                return tuple_visit<I - 1>::visit(std::forward<Visitor>(vis), tup, idx);
        }

        template <typename Visitor, typename Tuple>
        static auto visit(Visitor&& vis, Tuple&& tup, size_t idx)
        {
            if (idx == I - 1)
                return vis(std::get<I - 1>(tup));
            else
                return tuple_visit<I - 1>::visit(std::forward<Visitor>(vis), std::forward<Tuple>(tup), idx);
        }
    };

    template <>
    struct tuple_visit<1>
    {
        template <typename Visitor, typename Tuple>
        static auto visit(Visitor&& vis, const Tuple& tup, size_t idx)
        {
            return vis(std::get<0>(tup));
        }

        template <typename Visitor, typename Tuple>
        static auto visit(Visitor&& vis, Tuple&& tup, size_t idx)
        {
            return vis(std::get<0>(tup));
        }
    };
}   // namespace

template <typename Visitor, typename... Ts>
constexpr auto tuple_visit(Visitor&& vis, const std::tuple<Ts...>& tup, size_t idx)
{
    return _impl::tuple_visit<sizeof...(Ts)>::visit(std::forward<Visitor>(vis), tup, idx);
}

template <typename Visitor, typename... Ts>
constexpr auto tuple_visit(Visitor&& vis, std::tuple<Ts...>&& tup, size_t idx)
{
    return _impl::tuple_visit<sizeof...(Ts)>::visit(
        std::forward<Visitor>(vis), std::forward<std::tuple<Ts...>>(tup), idx);
}

// index_visit

namespace _impl
{
    template <size_t I>
    struct index_visit
    {
        template <typename Visitor>
        static auto visit(Visitor&& vis, std::size_t idx)
        {
            using Idx = std::integral_constant<std::size_t, I - 1>;
            if (idx == Idx::value)
                return vis(Idx{});
            else
                return index_visit<Idx::value>::visit(std::forward<Visitor>(vis), idx);
        }
    };

    template <>
    struct index_visit<1>
    {
        template <typename Visitor>
        static auto visit(Visitor&& vis, std::size_t)
        {
            using Idx = std::integral_constant<std::size_t, 0>;
            return vis(Idx{});
        }
    };
}   // namespace _impl

template <std::size_t Size, class Visitor>
constexpr auto index_visit(Visitor&& vis, size_t idx)
{
    return _impl::index_visit<Size>::visit(std::forward<Visitor>(vis), idx);
}

// tuple_visit_idx

template <class Tuple, class Visitor>
constexpr auto tuple_visit_idx(Visitor&& vis, size_t idx)
{
    return index_visit<std::tuple_size_v<Tuple>>(std::forward<Visitor>(vis), idx);
}

#endif
