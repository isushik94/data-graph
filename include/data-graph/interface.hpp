
#ifndef __DATA_GRAPH__INTERFACE_HPP__
#define __DATA_GRAPH__INTERFACE_HPP__

#include <utility>

namespace dg
{
    enum class interface_type
    {
        INLINE,
        UNIQUE
    };

    template <interface_type Type>
    struct interface
    {
        template <class Internal>
        struct base
        {
            using internal_type = Internal;

            inline operator bool() const noexcept;

            inline std::size_t hash() const noexcept;

        protected:
            base();
            ~base();

            template <class... Args>
            explicit base(std::in_place_t, Args&&... args);

            internal_type* self() noexcept;
            const internal_type* self() const noexcept;
        };

        template <class Interface>
        struct internal
        {
            using interface_type = Interface;
            using internal_type  = typename interface_type::internal_type;

            static internal_type* cast(interface_type& o);
            static const internal_type* cast(const interface_type& o);
        };
    };
}   // namespace dg

#include <data-graph/interface/inline.hpp>
#include <data-graph/interface/unique.hpp>

#endif
