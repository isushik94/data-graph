
#ifndef __DATA_GRAPH__ENGINE_HPP__
#define __DATA_GRAPH__ENGINE_HPP__

namespace dg
{
    namespace declarations
    {
        /**
         * @class data_type
         * @brief
         *
         * @tparam Ts
         */

        template <typename... Ts>
        struct data_type;

        /**
         * @class operation
         * @brief
         *
         * @tparam F
         */

        template <class F>
        struct operation;
    }   // namespace declarations

    /**
     * @using decl_data_type
     * @brief
     *
     * @tparam Ts
     */

    template <typename... Ts>
    using decl_data_type = declarations::data_type<Ts...>;

    /**
     * @using decl_operation
     * @brief
     *
     * @tparam F
     */

    template <class F>
    using decl_operation = declarations::operation<F>;

    /**
     * @class use_data_types
     * @brief
     *
     * @tparam Types
     */

    template <class... Types>
    struct use_data_types;

    /**
     * @class use_operations
     * @brief
     *
     * @tparam Operations
     */

    template <class... Operations>
    struct use_operations;

    /**
     * @class engine
     * @brief
     *
     * @tparam DataTypesList
     * @tparam OperationsList
     * @tparam Allocator
     */

    template <class DataTypesList, class OperationsList, class Allocator = std::allocator<std::byte>>
    struct engine;

    /**
     * @class graph
     * @brief
     *
     * @tparam Engine
     */

    template <class Engine>
    struct graph;

    /**
     * @class device
     * @brief
     *
     * @tparam Engine
     */

    template <class Engine>
    struct device;
}   // namespace dg

#include <data-graph/engine/main.hpp>

#include <data-graph/engine/graph.hpp>

#include <data-graph/engine/device.hpp>

#endif
