
#ifndef __DATA_GRAPH__INTERFACE__INLINE__INTERNAL_IPP__
#define __DATA_GRAPH__INTERFACE__INLINE__INTERNAL_IPP__

#include "../inline.hpp"   // For IDEs

namespace dg
{
    //
    // public
    //

    // static internal_type* cast(interface_type& o);

    template <class Interface>
    typename interface_inline::internal<Interface>::internal_type*
        interface_inline::internal<Interface>::cast(interface_type& o)
    {
        return o.self();
    }

    // static const internal_type* cast(const interface_type& o);

    template <class Interface>
    const typename interface_inline::internal<Interface>::internal_type*
        interface_inline::internal<Interface>::cast(const interface_type& o)
    {
        return o.self();
    }
}   // namespace dg

#endif