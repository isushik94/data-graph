
#ifndef __DATA_GRAPH__INTERFACE__INLINE__BASE_IPP__
#define __DATA_GRAPH__INTERFACE__INLINE__BASE_IPP__

#include "../inline.hpp"   // For IDEs

namespace dg
{
    //
    // public
    //

    // inline operator bool() const noexcept;

    template <class Internal>
    inline interface_inline::base<Internal>::operator bool() const noexcept
    {
        return bool(_self);
    }

    // inline std::size_t hash() const noexcept;

    template <class Internal>
    inline std::size_t interface_inline::base<Internal>::hash() const noexcept
    {
        return std::hash<decltype(_self)>()(_self);
    }

    //
    // protected
    //

    // explicit base(const internal_type& s);

    template <class Internal>
    interface_inline::base<Internal>::base(const internal_type& s) : _self(s)
    {}

    // explicit base(internal_type&& s) noexcept;

    template <class Internal>
    interface_inline::base<Internal>::base(internal_type&& s) noexcept : _self(std::move(s))
    {}

    // explicit base(std::in_place_t, Args&&... args);

    template <class Internal>
    template <class... Args>
    interface_inline::base<Internal>::base(std::in_place_t, Args&&... args)
        : _self(std::in_place, std::forward<Args>(args)...)
    {}

    // internal_type* self() noexcept;

    template <class Internal>
    Internal* interface_inline::base<Internal>::self() noexcept
    {
        if (_self)
            return _self.operator->();
        else
            return nullptr;
    }

    // const internal_type* self() const noexcept;

    template <class Internal>
    const Internal* interface_inline::base<Internal>::self() const noexcept
    {
        if (_self)
            return _self.operator->();
        else
            return nullptr;
    }
}   // namespace dg

namespace std
{
    // friend struct ::std::hash<dg::interface_inline::base<Internal>>;

    template <class Internal>
    struct hash<dg::interface_inline::base<Internal>>
    {
        typedef dg::interface_inline::base<Internal> argument_type;
        typedef std::size_t result_type;

        result_type operator()(const argument_type& i) const noexcept { return i.hash(); }
    };
}   // namespace std

#endif
