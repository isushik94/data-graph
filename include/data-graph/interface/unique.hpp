
#ifndef __DATA_GRAPH__INTERFACE__UNIQUE_HPP__
#define __DATA_GRAPH__INTERFACE__UNIQUE_HPP__

#ifndef __DATA_GRAPH__INTERFACE_HPP__
#    error "Please use `interface.hpp` instead. This header is not supposed to be used directly."
#endif

#include <memory>

namespace dg
{
    //
    // interface_type::UNIQUE
    //

    template <>
    struct interface<interface_type::UNIQUE>
    {
        template <class Interface>
        struct internal;

        /**
         * @class base
         */

        template <class Internal, class Allocator = std::allocator<Internal>>
        struct base
        {
            using internal_type  = Internal;
            using allocator_type = Allocator;

            inline operator bool() const noexcept;

            inline std::size_t hash() const noexcept;

            base(const base& o) = delete;
            base& operator=(const base& o) = delete;

        protected:
            base() = default;

            template <class... Args>
            explicit base(std::in_place_t, Args&&... args);

            template <class... Args>
            base(std::allocator_arg_t, allocator_type alloc, Args&&... args);

            ~base() = default;

            base(base&& o) noexcept = default;
            base& operator=(base&& o) noexcept = default;

            internal_type* self() noexcept;
            const internal_type* self() const noexcept;

        private:
            struct _deleter;
            using _ptr_t = std::unique_ptr<internal_type, _deleter>;

            _ptr_t _self;

            template <class>
            friend struct internal;

            friend struct ::std::hash<base>;
        };

        /**
         * @class internal
         */

        template <class Interface>
        struct internal
        {
            using interface_type = Interface;
            using internal_type  = typename interface_type::internal_type;
            using ptr_t          = typename interface_type::_ptr_t;

            static internal_type* cast(interface_type& o);
            static const internal_type* cast(const interface_type& o);

            static internal_type* cast(ptr_t& o);
            static const internal_type* cast(const ptr_t& o);

        protected:
            internal()  = default;
            ~internal() = default;
        };
    };

    using interface_unique = interface<interface_type::UNIQUE>;
}   // namespace dg

#include "./unique/base.ipp"
#include "./unique/internal.ipp"

#endif
