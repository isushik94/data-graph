
#ifndef __DATA_GRAPH__INTERFACE__UNIQUE__BASE_IPP__
#define __DATA_GRAPH__INTERFACE__UNIQUE__BASE_IPP__

#include "../unique.hpp"   // for IDEs

namespace dg
{
    //
    // public
    //

    // inline operator bool() const noexcept;

    template <class Internal, class Allocator>
    inline interface_unique::base<Internal, Allocator>::operator bool() const noexcept
    {
        return bool(_self);
    }

    // inline std::size_t hash() const noexcept;

    template <class Internal, class Allocator>
    inline std::size_t interface_unique::base<Internal, Allocator>::hash() const noexcept
    {
        return std::hash<decltype(_self)>()(_self);
    }

    //
    // protected
    //

    // struct _deleter;

    template <class Internal, class Allocator>
    struct interface_unique::base<Internal, Allocator>::_deleter
    {
        using allocator_type = typename base::allocator_type;

        _deleter() {}
        _deleter(allocator_type&& a) : _a(std::move(a)) {}

        _deleter(_deleter&& d) : _a(std::move(d._a)) {}

        _deleter& operator=(_deleter&& d) noexcept
        {
            _a = std::move(d._a);
            return *this;
        }

        _deleter(const _deleter& d) = delete;
        _deleter& operator=(const _deleter& d) = delete;

        ~_deleter() = default;

        void operator()(internal_type* ptr)
        {
            if (_a && ptr)
            {
                std::allocator_traits<allocator_type>::destroy(*_a, ptr);
                std::allocator_traits<allocator_type>::deallocate(*_a, ptr, 1);
            }
        }

        allocator_type get_allocator() const { return _a; }

    private:
        std::optional<allocator_type> _a;
    };

    // explicit base(std::in_place_t, Args&&... args);

    template <class Internal, class Allocator>
    template <class... Args>
    interface_unique::base<Internal, Allocator>::base(std::in_place_t, Args&&... args)
        : base(std::allocator_arg, allocator_type(), std::forward<Args>(args)...)
    {}

    // base(std::allocator_arg_t, allocator_type alloc, Args&&... args);

    template <class Internal, class Allocator>
    template <class... Args>
    interface_unique::base<Internal, Allocator>::base(std::allocator_arg_t, allocator_type alloc, Args&&... args)
    {
        auto ptr = std::allocator_traits<allocator_type>::allocate(alloc, 1);

        try
        {
            std::allocator_traits<allocator_type>::construct(alloc, ptr, std::forward<Args>(args)...);
            _self = _ptr_t(ptr, _deleter(std::move(alloc)));
        }
        catch (...)
        {
            std::allocator_traits<allocator_type>::deallocate(alloc, ptr, 1);
            throw;
        }
    }

    // internal_type* self() noexcept;

    template <class Internal, class Allocator>
    Internal* interface_unique::base<Internal, Allocator>::self() noexcept
    {
        return _self.get();
    }

    // const internal_type* self() const noexcept;

    template <class Internal, class Allocator>
    const Internal* interface_unique::base<Internal, Allocator>::self() const noexcept
    {
        return _self.get();
    }
}   // namespace dg

namespace std
{
    // friend struct ::std::hash<:dg::interface_unique::base<Internal, Allocator>>;

    template <class Internal, class Allocator>
    struct hash<::dg::interface_unique::base<Internal, Allocator>>
    {
        typedef ::dg::interface_unique::base<Internal, Allocator> argument_type;
        typedef std::size_t result_type;

        result_type operator()(const argument_type& i) const noexcept { return i.hash(); }
    };
}   // namespace std

#endif