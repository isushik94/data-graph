
#ifndef __DATA_GRAPH__INTERFACE__UNIQUE__INTERNAL_IPP__
#define __DATA_GRAPH__INTERFACE__UNIQUE__INTERNAL_IPP__

#include "../unique.hpp"   // for IDEs

namespace dg
{
    //
    // public
    //

    // static internal_type* cast(interface_type& o);

    template <class Interface>
    typename interface_unique::internal<Interface>::internal_type*
        interface_unique::internal<Interface>::cast(interface_type& o)
    {
        return o._self.get();
    }

    // static const internal_type* cast(const interface_type& o);

    template <class Interface>
    const typename interface_unique::internal<Interface>::internal_type*
        interface_unique::internal<Interface>::cast(const interface_type& o)
    {
        return o._self.get();
    }

    // static internal_type* cast(ptr_t& o);

    template <class Interface>
    typename interface_unique::internal<Interface>::internal_type* interface_unique::internal<Interface>::cast(ptr_t& o)
    {
        return o.get();
    }

    // static const internal_type* cast(const ptr_t& o);

    template <class Interface>
    const typename interface_unique::internal<Interface>::internal_type*
        interface_unique::internal<Interface>::cast(const ptr_t& o)
    {
        return o.get();
    }
}   // namespace dg

#endif
